json.extract! stock, :id, :product_name, :description, :quantity, :price, :created_at, :updated_at
json.url stock_url(stock, format: :json)
