class StockPolicy < ApplicationPolicy
  def new?
    false
  end
  
  def create?
    new?
  end

  def edit?
    #client 
    false
  end
  
  def update?
    edit?
  end
  
  def destroy?
    edit?
  end

  class Scope < Scope
    def resolve
      if user.nil?
        scope.all
      else
        raise Pundit::NotAuthorizedError
      end
    end
  end
end