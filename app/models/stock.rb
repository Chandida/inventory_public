class Stock < ApplicationRecord
  validates :product_name, presence: true
  validates :quantity, presence: true
  validates :description, presence: true
  validates :price, presence: true
  paginates_per 5
     def self.ransackable_attributes(auth_object = nil)
    ["created_at", "description", "id", "price", "product_name", "quantity", "updated_at"]
  end
end
