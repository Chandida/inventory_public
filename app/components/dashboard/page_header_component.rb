# frozen_string_literal: true

class Dashboard::PageHeaderComponent < ViewComponent::Base
      def initialize(title:, link: nil, action: false,q: nil)
    @title = title
    @link = link
    @action = action
    @q=q
  end
end
