# frozen_string_literal: true

class Content::StockComponent < ViewComponent::Base
    with_collection_parameter :stock

    def initialize(stock:)
    @stock = stock
  end
end
