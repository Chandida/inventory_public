class StocksController < ApplicationController
  before_action :set_stock, only: %i[ show edit update destroy ]
  skip_after_action :verify_policy_scoped, only: :index
  skip_after_action :verify_authorized, except: :index
  before_action :authenticate

  # GET /stocks or /stocks.json
  def index
    @q = Stock.ransack(params[:q])
    @stocks = @q.result(distinct: true).page params[:page]
    # @stocks = Stock.all
  end

  # GET /stocks/1 or /stocks/1.json
  def show
  end

  # GET /stocks/new
  def new
    @stock = Stock.new
    render turbo_stream: turbo_stream.update("model", partial: "shared/form", locals: {url: stocks_path,object: Stock.new})
  end

  # GET /stocks/1/edit
  def edit
    render turbo_stream: turbo_stream.update("model", partial: "shared/form", locals: {url: stock_path,object: @stock})
  end

  # POST /stocks or /stocks.json
  def create
    @stock = Stock.new(stock_params)
    @stock.account_id = current_account.id
    respond_to do |format|
      if @stock.save
        format.html { redirect_to stocks_path, notice: "Stock was successfully created." }
        format.turbo_stream { }
      else
        format.html { render :new, status: :unprocessable_entity }
       format.turbo_stream { }
      end
    end
  end

  # PATCH/PUT /stocks/1 or /stocks/1.json
  def update
    respond_to do |format|
      if @stock.update(stock_params)
      index
        format.html { redirect_to stocks_url, notice: "Stock was successfully updated." }
        format.json { render :show, status: :ok, location: @stock }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @stock.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stocks/1 or /stocks/1.json
  def destroy
    @stock.destroy

    respond_to do |format|
      format.html { redirect_to stocks_url, notice: "Stock was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stock
      @stock = Stock.find(params[:id])

    end

    # Only allow a list of trusted parameters through.
    def stock_params
      params.require(:stock).permit(:product_name, :description, :quantity, :price)
    end
end
