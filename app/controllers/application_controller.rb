class ApplicationController < ActionController::Base
    include Pundit::Authorization
    after_action :verify_authorized, except: :index

    before_action :initialize_breadcrumb
    after_action :verify_policy_scoped, only: :index

    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

    around_action :switch_locale
    
 # Overwrite unverified request handler to force a refresh / redirect.
  def handle_unverified_request
    redirect_to request.referrer
  end

  def user_not_authorized
      flash[:alert] = I18n.t("errors.unauthorized")
      redirect_back_or_to root_path, allow_other_host: false
      rescue ActionController::Redirecting::UnsafeRedirectError
      redirect_to root_path
  end

  #Setting the Locale from User Preferences
  def switch_locale(&action)
    locale = current_account.try(:locale) || I18n.default_locale
    I18n.with_locale(locale, &action)
  end

  
  def default_url_options
    { locale: I18n.locale }
  end
  
  protected

    def authenticate
        rodauth.require_account # redirect to login page if not authenticated
    end

    def pundit_user
        current_account
    end

    def initialize_breadcrumb
        @breadcrumb_active_page = ""
        @breadcrumb_hash = {}
    end
end
