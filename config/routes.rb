Rails.application.routes.draw do
  # resources :stocks
 # get #'home/index'
  namespace :api do 
    namespace :v1 do
      resources :stocks
    end
  end
  
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  #get "/auth/:provider/callback", to: "rodauth#omniauth"
  # Defines the root path route ("/")
  root "stocks#index"
  scope '/dashboard' do 
    get "/dashboard", to: "stocks#index"
    resources :stocks
  end
end
